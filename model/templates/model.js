// This is a file copied by your subgenerator.

define([
    "app"
    ,"kendoui-web"
]
    , function (app, kendo) {

        var <%= UpName %>Model = kendo.data.Model.define({
            id: "id"
            , fields: {
                id: { nullable: true }
            }

        });
        return {};
    });