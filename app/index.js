'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');


var ItcubeGenerator = module.exports = function ItcubeGenerator(args, options, config) {
  yeoman.generators.Base.apply(this, arguments);

  this.on('end', function () {
    //this.installDependencies({ skipInstall: options['skip-install'] });
  });

  //this.pkg = JSON.parse(this.readFileAsString(path.join(__dirname, '../package.json')));
};

util.inherits(ItcubeGenerator, yeoman.generators.Base);

ItcubeGenerator.prototype.askFor = function askFor() {
  var cb = this.async();

  // have Yeoman greet the user.
  console.log(this.yeoman);

  var prompts = [{
    //type: 'confirm',
    name: 'moduleName',
    message: 'Введите название модуля',
    default: 'name'
  }];

  this.prompt(prompts, function (props) {
    
    this.moduleName = props.moduleName;
    this.UpModuleName = props.moduleName.charAt(0).toUpperCase() + props.moduleName.substring(1, props.moduleName.length);
    this.LoverModuleName = props.moduleName.toLowerCase();

    cb();
  }.bind(this));
};

ItcubeGenerator.prototype.app = function app() {
  //folders
  this.mkdir(this.moduleName);
  this.mkdir(this.moduleName+'/dataSources');
  this.mkdir(this.moduleName+'/models');
  this.mkdir(this.moduleName+'/views');
  //templates
  this.template('nameDataSource.js',this.moduleName+'/dataSources/'+this.moduleName+'DataSource.js');
  this.template('nameModel.js',this.moduleName+'/models/'+this.moduleName+'Model.js');
  this.template('nameMainView.js',this.moduleName+'/views/'+this.moduleName+'MainView.js');
  this.template('nameRouter.js',this.moduleName+'/'+this.moduleName+'Router.js');
  this.template('nameModule.js',this.moduleName+'/'+this.moduleName+'Module.js');


  //this.copy('_package.json', 'package.json');
  //this.copy('_bower.json', 'bower.json');
};

ItcubeGenerator.prototype.projectfiles = function projectfiles() {
  //this.copy('editorconfig', '.editorconfig');
  //this.copy('editorconfig', '.editorconfig');
  //this.copy('jshintrc', '.jshintrc');
};
