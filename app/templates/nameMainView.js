// <%= moduleName %>
    define([
    "app"
    , "../<%= LoverModuleName %>Module"
    , "../dataSources/<%= LoverModuleName %>DataSource"
    , "kendoui-web"
], function(app, <%= UpModuleName %>, <%= UpModuleName %>DataSource) {
    
    <%= UpModuleName %>.Views.Main = Backbone.Layout.extend({
         el: "#main"
        , template: "<%= LoverModuleName %>/main"
        , columns: [
            // поля показанные в таблице
            {
                field:'id'
                , title: 'id'
                , width:'50px'
                , editor: function(container, options) {
                    //скрываем поле из формы редактирование удаления
                    container.hide();
                    container.prev().hide();
                }
            }
            , {field:'lastName', title: 'Фамилия'}
            , {field:'firstName', title: 'Имя'}
            , {field:'createDate', title: 'Дата создания'}
            // скрытые поля
            , {field:'email', title: 'E-mail', hidden: true}
            , {field:'phone', title: 'телефон', hidden: true}
            , { command: [
                {name: "destroy"}
                , {name: "edit"}
            ]}
        ]
        , afterRender: function() {
            this.$('.grid').kendoGrid({
                columns: this.columns
                , toolbar: ['create']
                , editable: 'popup'
                , dataSource: <%= UpModuleName %>DataSource
            });
        }
    });
    return <%= UpModuleName %>;
});