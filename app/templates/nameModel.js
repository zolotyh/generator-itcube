// <%= moduleName %>

define([
    "app"
    ,"kendoui-web"
]
    , function (app, kendo) {

        var <%= UpModuleName %>Model = kendo.data.Model.define({
            id: "id"
            , fields: {
                id: { nullable: true }
            }

        });
        return <%= UpModuleName %>Model;
    });