// <%= moduleName %>

define([
    'app'
    , './<%= LoverModuleName %>Module'
    , './views/<%= LoverModuleName %>MainView'
    , "backbone.subroute"
], function (app, <%= UpModuleName %>) {

    <%= UpModuleName %>.Router = Backbone.SubRoute.extend({
        routes: {
            "": "main"
        },
        "main": function () {
            var layout = new <%= UpModuleName %>.Views.Main();
            layout.render();
        }
    });


    return <%= UpModuleName %>;
});