// <%= moduleName %>, <%= UpModuleName %>, <%= LoverModuleName %> 

define([
    "app"
], function(app) {
    var <%= UpModuleName %> = new app.module();
    
    return <%= UpModuleName %>;
});