'use strict';
var util = require('util');
var yeoman = require('yeoman-generator');

var ViewGenerator = module.exports = function ViewGenerator(args, options, config) {
  // By calling `NamedBase` here, we get the argument to the subgenerator call
  // as `this.name`.
  yeoman.generators.NamedBase.apply(this, arguments);
    
    //создаем нужный формат слова 
    this.UpName = this.name.charAt(0).toUpperCase() + this.name.substring(1, this.name.length);
    this.LoverName = this.name.toLowerCase();
    
    console.log('You called the view subgenerator with the argument ' + this.name + '.');
};

util.inherits(ViewGenerator, yeoman.generators.NamedBase);

ViewGenerator.prototype.files = function files() {
  this.template('view.js', this.name+'View.js');
};
