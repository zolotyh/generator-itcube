// <%= UpName %>, <%= LoverName %>,
    define([
    "app"
    , "kendoui-web"
], function(app) {
    
    Default.Views.<%= UpName %> = Backbone.Layout.extend({
         el: "#main"
        , template: "default/<%= LoverName %>"
        , columns: [
            // поля показанные в таблице
            {
                field:'id'
                , title: 'id'
                , width:'40px'
                , editor: function(container, options) {
                    //скрываем поле из формы редактирование удаления
                    container.hide();
                    container.prev().hide();
                }
            }
            , {field:'lastName', title: 'Фамилия'}
            , {field:'firstName', title: 'Имя'}
            , {field:'createDate', title: 'Дата создания'}
            // скрытые поля
            , {field:'email', title: 'E-mail', hidden: true}
            , { command: [
                {name: "destroy"}
                , {name: "edit"}
            ]}
        ]
        , afterRender: function() {
            this.$('.grid').kendoGrid({
                columns: this.columns
                , toolbar: ['create']
                , editable: 'popup'
                , dataSource: [
                    {id:1,lastName:"Фамилия 1", firstName:"Имя 1",email:"email@itcube.ru"}]
                    , {id:2,lastName:"Фамилия 2", firstName:"Имя 3",email:"email@itcube.ru"}]
                    , {id:3,lastName:"Фамилия 3", firstName:"Имя 4",email:"email@itcube.ru"}]
            });
        }
    });
    return {};
});