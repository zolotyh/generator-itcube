'use strict';
var util = require('util');
var yeoman = require('yeoman-generator');

var DatasourceGenerator = module.exports = function DatasourceGenerator(args, options, config) {
  // By calling `NamedBase` here, we get the argument to the subgenerator call
  // as `this.name`.
    yeoman.generators.NamedBase.apply(this, arguments);
    
    this.UpName = this.name.charAt(0).toUpperCase() + this.name.substring(1, this.name.length);
    this.LoverName = this.name.toLowerCase();

    console.log('You called the dataSource subgenerator with the argument ' + this.name + '.');
};

util.inherits(DatasourceGenerator, yeoman.generators.NamedBase);

DatasourceGenerator.prototype.files = function files() {
  this.template('dataSource.js', this.LoverName+'DataSource.js');
};
