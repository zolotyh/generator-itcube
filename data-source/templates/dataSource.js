define([
    "app"
    , "../models/<%= LoverName %>Model"
    , "kendoui-web"
]
    , function (app, <%= UpName %>Model) {

        var <%= UpName %>DataSource = new kendo.data.DataSource({
            transport: {
                read:  {
                    url: app.root + app.api + "<%= LoverName %>"
                    , type: "GET"
                    , dataType: "json"
                }
                , update: {
                    url: function(options) {
                        return app.root + app.api + "<%= LoverName %>/"+options.id;
                    }
                    , type: "PUT"
                    , dataType: "json"
                    , contentType: "application/json"
                }
                , destroy: {
                    url: function(options) {
                        return app.root + app.api + "<%= LoverName %>/"+options.id;
                    }
                    , type: "DELETE"
                    , contentType: "application/json"
                }
                , create: {
                    url: app.root + app.api + "<%= LoverName %>"
                    , type: "POST"
                    , contentType: "application/json"
                }

                ,parameterMap: function(options, operation) {
                    console.log('parameterMap',options, operation);
                    if (operation == "read") {
                        return {
                            offset: options.skip
                            , limit: options.take
                        };
                    } else if(operation == "update") {
                        return  kendo.stringify(options);
                    } else if(operation == "create") {
                        delete options.id
                        return kendo.stringify(options);
                    }
                }

            }
            , batch: false
            , serverPaging: true
            , pageSize: 20
            , sync: function(e) {
                console.log("sync complete", arguments);
                this.read();
            }
            , error: function() {
                console.log('ошибка');
            }
            , schema: {
                model: <%= UpName %>Model
                , parse: function(response) {
                    return response.result;
                }
                , total: function(response) {
                    return response.total;
                }

            }

        });





        return {};
    });