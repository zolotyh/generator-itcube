define([
    "app"
], function(app) {
    
    var <%= UpName %> = new app.module();
    
    return <%= UpName %>;
});