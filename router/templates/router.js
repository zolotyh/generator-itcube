// This is a file copied by your subgenerator.

define([
    'app'
    ,'./<%= LoverName %>Module'
    , './views/<%= LoverName %>MainView'
    , "backbone.subroute"
],function (app,<%= UpName %>) {

    Default.Router = Backbone.SubRoute.extend({
        routes: {
            "": "main"
        }
        , "main": function() {
           console.log('main default');
            var layout = new <%= UpName %>.Views.Main();
            layout.render();
        }
    });


    return <%= UpName %>;
});